# build with sudo docker build --pull -t mint-unison .
# run with sudo docker run -v $HOME/.unison:$HOME/.unison -v $HOME/.ssh:$HOME/.ssh:ro -v /data:/data -v $HOME/.Xauthority:$HOME/.Xauthority --env="DISPLAY" --net=host --name mint -ti mint-unison bash
FROM linuxmintd/mint20-amd64
RUN apt-get install -y unison-gtk

RUN useradd munzner
USER munzner
